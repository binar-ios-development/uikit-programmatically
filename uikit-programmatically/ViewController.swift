//
//  ViewController.swift
//  uikit-programmatically
//
//  Created by Arie May Wibowo on 09/02/22.
//

import UIKit

class ViewController: UIViewController {
    
    var labelNama: UILabel!
    var labelKeteranganNama: UILabel!
    var inputLabelNama: UITextField!
    
    override func loadView() {
        view = UIView()
        view.backgroundColor = .white
        setAllComponent()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        labelNama.text = inputLabelNama.text
    }
    
    func setAllComponent() -> Void {
        labelNama = UILabel()
        labelNama.translatesAutoresizingMaskIntoConstraints = false
        labelNama.textAlignment = .center
        labelNama.font = UIFont.systemFont(ofSize: 24, weight: .bold)
        labelNama.text = "Hello"
        view.addSubview(labelNama)
        
        labelKeteranganNama = UILabel()
        labelKeteranganNama.translatesAutoresizingMaskIntoConstraints = false
        labelKeteranganNama.textAlignment = .left
        labelKeteranganNama.text = "Masukkan nama kalian"
        view.addSubview(labelKeteranganNama)
        
        inputLabelNama = UITextField()
        inputLabelNama.backgroundColor = .yellow
        inputLabelNama.sizeToFit()
        inputLabelNama.translatesAutoresizingMaskIntoConstraints = false
        inputLabelNama.placeholder = "Input your name here..."
        inputLabelNama.textAlignment = .left
        inputLabelNama.text = ""
        inputLabelNama.addTarget(self, action: #selector(ViewController.textFieldDidChange(_:)), for: .editingChanged)
        view.addSubview(inputLabelNama)
        
        let margins = view.layoutMarginsGuide
        NSLayoutConstraint.activate([
            labelNama.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor, constant: 50),
            labelNama.centerXAnchor.constraint(equalTo: view.layoutMarginsGuide.centerXAnchor),
            
            labelKeteranganNama.topAnchor.constraint(equalTo: labelNama.bottomAnchor, constant: 60),
            labelKeteranganNama.leadingAnchor.constraint(equalTo: margins.leadingAnchor),
            labelKeteranganNama.trailingAnchor.constraint(equalTo: margins.trailingAnchor),
            
            inputLabelNama.topAnchor.constraint(equalTo: labelKeteranganNama.bottomAnchor, constant: 20),
            inputLabelNama.leadingAnchor.constraint(equalTo: margins.leadingAnchor),
            inputLabelNama.trailingAnchor.constraint(equalTo: margins.trailingAnchor),
            inputLabelNama.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        labelNama.text = textField.text
    }

}

