//
//  SliderViewController.swift
//  uikit-programmatically
//
//  Created by Arie May Wibowo on 22/02/22.
//

import UIKit

class SliderViewController: UIViewController {
    
    var slider: UISlider!
    var nilaiSlider: UILabel!
    var image: UIImageView!
    var imageContainer: UIView!
    
    override func loadView() {
        view = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        view.backgroundColor = .white
        setUpView()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    func setUpView() -> Void {
        let imgView = UIView()
        imgView.frame.size.width = view.frame.width
        imgView.frame.size.height = view.frame.height / 2
                
        let catImage = UIImage(named: "women.jpg")
                
        image = UIImageView()
        image.contentMode = UIView.ContentMode.scaleAspectFill
        image.frame.size.width = 200
        image.frame.size.height = 400
        image.center = imgView.center
        image.image = catImage
        imgView.addSubview(image)
        view.addSubview(imgView)
        
        
        
        NSLayoutConstraint.activate([
            image.centerXAnchor.constraint(equalTo: imgView.centerXAnchor)
        ])
    }
}
